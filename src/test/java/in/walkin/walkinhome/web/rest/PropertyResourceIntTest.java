package in.walkin.walkinhome.web.rest;

import in.walkin.walkinhome.Application;
import in.walkin.walkinhome.domain.Property;
import in.walkin.walkinhome.domain.PropertyDetails;
import in.walkin.walkinhome.domain.enumeration.FoodPref;
import in.walkin.walkinhome.domain.enumeration.Status;
import in.walkin.walkinhome.domain.enumeration.TenantType;
import in.walkin.walkinhome.repository.PropertyRepository;
import in.walkin.walkinhome.service.PropertyService;
import in.walkin.walkinhome.service.dto.PropertyDTO;
import in.walkin.walkinhome.service.mapper.PropertyMapper;
import in.walkin.walkinhome.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static in.walkin.walkinhome.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PropertyResource REST controller.
 *
 * @see PropertyResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class PropertyResourceIntTest {

    private static final Status DEFAULT_STATUS = Status.BOOKED;
    private static final Status UPDATED_STATUS = Status.AVAILABLE;

    private static final Float DEFAULT_AREA = 1F;
    private static final Float UPDATED_AREA = 2F;

    private static final String DEFAULT_DESC = "AAAAAAAAAA";
    private static final String UPDATED_DESC = "BBBBBBBBBB";

    private static final Integer DEFAULT_CAR_PARKING_NO = 1;
    private static final Integer UPDATED_CAR_PARKING_NO = 2;

    private static final Integer DEFAULT_TWO_WHEELER_PARKING_NO = 1;
    private static final Integer UPDATED_TWO_WHEELER_PARKING_NO = 2;

    private static final Double DEFAULT_PRICE = 1D;
    private static final Double UPDATED_PRICE = 2D;

    private static final Integer DEFAULT_BUILT_YEAR = 1;
    private static final Integer UPDATED_BUILT_YEAR = 2;

    private static final Integer DEFAULT_FLOOR = 1;
    private static final Integer UPDATED_FLOOR = 2;

    private static final TenantType DEFAULT_TENANT_TYPE = TenantType.GIRL;
    private static final TenantType UPDATED_TENANT_TYPE = TenantType.BOY;

    private static final FoodPref DEFAULT_FOOD_PREF = FoodPref.VEG;
    private static final FoodPref UPDATED_FOOD_PREF = FoodPref.NON_VEG;

    private static final Boolean DEFAULT_IS_SECURITY = false;
    private static final Boolean UPDATED_IS_SECURITY = true;

    private static final Boolean DEFAULT_IS_LIFT = false;
    private static final Boolean UPDATED_IS_LIFT = true;

    private static final Integer DEFAULT_NO_OF_FLOORS = 1;
    private static final Integer UPDATED_NO_OF_FLOORS = 2;

    private static final Integer DEFAULT_NEARBY_HOSPITAL = 1;
    private static final Integer UPDATED_NEARBY_HOSPITAL = 2;

    private static final Integer DEFAULT_NEARBY_SHOP = 1;
    private static final Integer UPDATED_NEARBY_SHOP = 2;

    private static final Integer DEFAULT_NEARBY_BUSSTOP = 1;
    private static final Integer UPDATED_NEARBY_BUSSTOP = 2;


    @Autowired
    private PropertyRepository propertyRepository;


    @Autowired
    private PropertyMapper propertyMapper;
    

    @Autowired
    private PropertyService propertyService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPropertyMockMvc;

    private Property property;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PropertyResource propertyResource = new PropertyResource(propertyService);
        this.restPropertyMockMvc = MockMvcBuilders.standaloneSetup(propertyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Property createEntity(EntityManager em) {
        PropertyDetails propertyDetails = new PropertyDetails()
                .noOfFloors(DEFAULT_NO_OF_FLOORS)
                .nearbyHospital(DEFAULT_NEARBY_HOSPITAL)
                .nearbyShop(DEFAULT_NEARBY_SHOP)
                .nearbyBusstop(DEFAULT_NEARBY_BUSSTOP);

        Property property = new Property()
            .status(DEFAULT_STATUS)
            .area(DEFAULT_AREA)
            .desc(DEFAULT_DESC)
            .carParkingNo(DEFAULT_CAR_PARKING_NO)
            .twoWheelerParkingNo(DEFAULT_TWO_WHEELER_PARKING_NO)
            .price(DEFAULT_PRICE)
            .builtYear(DEFAULT_BUILT_YEAR)
            .floor(DEFAULT_FLOOR)
            .tenantType(DEFAULT_TENANT_TYPE)
            .foodPref(DEFAULT_FOOD_PREF)
            .isSecurity(DEFAULT_IS_SECURITY)
            .isLift(DEFAULT_IS_LIFT)
            .propertyDetails(propertyDetails);
        return property;
    }

    @Before
    public void initTest() {
        property = createEntity(em);
    }

    @Test
    @Transactional
    public void createProperty() throws Exception {
        int databaseSizeBeforeCreate = propertyRepository.findAll().size();

        // Create the Property
        PropertyDTO propertyDTO = propertyMapper.toDto(property);
        restPropertyMockMvc.perform(post("/api/properties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(propertyDTO)))
            .andExpect(status().isCreated());

        // Validate the Property in the database
        List<Property> propertyList = propertyRepository.findAll();
        assertThat(propertyList).hasSize(databaseSizeBeforeCreate + 1);
        Property testProperty = propertyList.get(propertyList.size() - 1);
        assertThat(testProperty.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testProperty.getArea()).isEqualTo(DEFAULT_AREA);
        assertThat(testProperty.getDesc()).isEqualTo(DEFAULT_DESC);
        assertThat(testProperty.getCarParkingNo()).isEqualTo(DEFAULT_CAR_PARKING_NO);
        assertThat(testProperty.getTwoWheelerParkingNo()).isEqualTo(DEFAULT_TWO_WHEELER_PARKING_NO);
        assertThat(testProperty.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testProperty.getBuiltYear()).isEqualTo(DEFAULT_BUILT_YEAR);
        assertThat(testProperty.getFloor()).isEqualTo(DEFAULT_FLOOR);
        assertThat(testProperty.getTenantType()).isEqualTo(DEFAULT_TENANT_TYPE);
        assertThat(testProperty.getFoodPref()).isEqualTo(DEFAULT_FOOD_PREF);
        assertThat(testProperty.isIsSecurity()).isEqualTo(DEFAULT_IS_SECURITY);
        assertThat(testProperty.isIsLift()).isEqualTo(DEFAULT_IS_LIFT);
/*        PropertyDetails testPropertyDetails = testProperty.getPropertyDetails();
        assertThat(testPropertyDetails.getNoOfFloors()).isEqualTo(DEFAULT_NO_OF_FLOORS);
        assertThat(testPropertyDetails.getNearbyHospital()).isEqualTo(DEFAULT_NEARBY_HOSPITAL);
        assertThat(testPropertyDetails.getNearbyShop()).isEqualTo(DEFAULT_NEARBY_SHOP);
        assertThat(testPropertyDetails.getNearbyBusstop()).isEqualTo(DEFAULT_NEARBY_BUSSTOP);*/
    }

    @Test
    @Transactional
    public void createPropertyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = propertyRepository.findAll().size();

        // Create the Property with an existing ID
        property.setId(1L);
        PropertyDTO propertyDTO = propertyMapper.toDto(property);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPropertyMockMvc.perform(post("/api/properties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(propertyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Property in the database
        List<Property> propertyList = propertyRepository.findAll();
        assertThat(propertyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllProperties() throws Exception {
        // Initialize the database
        propertyRepository.saveAndFlush(property);

        // Get all the propertyList
        restPropertyMockMvc.perform(get("/api/properties?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(property.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].area").value(hasItem(DEFAULT_AREA.doubleValue())))
            .andExpect(jsonPath("$.[*].desc").value(hasItem(DEFAULT_DESC.toString())))
            .andExpect(jsonPath("$.[*].carParkingNo").value(hasItem(DEFAULT_CAR_PARKING_NO)))
            .andExpect(jsonPath("$.[*].twoWheelerParkingNo").value(hasItem(DEFAULT_TWO_WHEELER_PARKING_NO)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].builtYear").value(hasItem(DEFAULT_BUILT_YEAR)))
            .andExpect(jsonPath("$.[*].floor").value(hasItem(DEFAULT_FLOOR)))
            .andExpect(jsonPath("$.[*].tenantType").value(hasItem(DEFAULT_TENANT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].foodPref").value(hasItem(DEFAULT_FOOD_PREF.toString())))
            .andExpect(jsonPath("$.[*].isSecurity").value(hasItem(DEFAULT_IS_SECURITY.booleanValue())))
            .andExpect(jsonPath("$.[*].isLift").value(hasItem(DEFAULT_IS_LIFT.booleanValue())));
    }
    

    @Test
    @Transactional
    public void getProperty() throws Exception {
        // Initialize the database
        propertyRepository.saveAndFlush(property);

        // Get the property
        restPropertyMockMvc.perform(get("/api/properties/{id}", property.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(property.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.area").value(DEFAULT_AREA.doubleValue()))
            .andExpect(jsonPath("$.desc").value(DEFAULT_DESC.toString()))
            .andExpect(jsonPath("$.carParkingNo").value(DEFAULT_CAR_PARKING_NO))
            .andExpect(jsonPath("$.twoWheelerParkingNo").value(DEFAULT_TWO_WHEELER_PARKING_NO))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.doubleValue()))
            .andExpect(jsonPath("$.builtYear").value(DEFAULT_BUILT_YEAR))
            .andExpect(jsonPath("$.floor").value(DEFAULT_FLOOR))
            .andExpect(jsonPath("$.tenantType").value(DEFAULT_TENANT_TYPE.toString()))
            .andExpect(jsonPath("$.foodPref").value(DEFAULT_FOOD_PREF.toString()))
            .andExpect(jsonPath("$.isSecurity").value(DEFAULT_IS_SECURITY.booleanValue()))
            .andExpect(jsonPath("$.isLift").value(DEFAULT_IS_LIFT.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingProperty() throws Exception {
        // Get the property
        restPropertyMockMvc.perform(get("/api/properties/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProperty() throws Exception {
        // Initialize the database
        propertyRepository.saveAndFlush(property);

        int databaseSizeBeforeUpdate = propertyRepository.findAll().size();

        // Update the property
        Property updatedProperty = propertyRepository.findById(property.getId()).get();
        // Disconnect from session so that the updates on updatedProperty are not directly saved in db
        em.detach(updatedProperty);
        updatedProperty
            .status(UPDATED_STATUS)
            .area(UPDATED_AREA)
            .desc(UPDATED_DESC)
            .carParkingNo(UPDATED_CAR_PARKING_NO)
            .twoWheelerParkingNo(UPDATED_TWO_WHEELER_PARKING_NO)
            .price(UPDATED_PRICE)
            .builtYear(UPDATED_BUILT_YEAR)
            .floor(UPDATED_FLOOR)
            .tenantType(UPDATED_TENANT_TYPE)
            .foodPref(UPDATED_FOOD_PREF)
            .isSecurity(UPDATED_IS_SECURITY)
            .isLift(UPDATED_IS_LIFT);
        PropertyDTO propertyDTO = propertyMapper.toDto(updatedProperty);

        restPropertyMockMvc.perform(put("/api/properties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(propertyDTO)))
            .andExpect(status().isOk());

        // Validate the Property in the database
        List<Property> propertyList = propertyRepository.findAll();
        assertThat(propertyList).hasSize(databaseSizeBeforeUpdate);
        Property testProperty = propertyList.get(propertyList.size() - 1);
        assertThat(testProperty.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testProperty.getArea()).isEqualTo(UPDATED_AREA);
        assertThat(testProperty.getDesc()).isEqualTo(UPDATED_DESC);
        assertThat(testProperty.getCarParkingNo()).isEqualTo(UPDATED_CAR_PARKING_NO);
        assertThat(testProperty.getTwoWheelerParkingNo()).isEqualTo(UPDATED_TWO_WHEELER_PARKING_NO);
        assertThat(testProperty.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testProperty.getBuiltYear()).isEqualTo(UPDATED_BUILT_YEAR);
        assertThat(testProperty.getFloor()).isEqualTo(UPDATED_FLOOR);
        assertThat(testProperty.getTenantType()).isEqualTo(UPDATED_TENANT_TYPE);
        assertThat(testProperty.getFoodPref()).isEqualTo(UPDATED_FOOD_PREF);
        assertThat(testProperty.isIsSecurity()).isEqualTo(UPDATED_IS_SECURITY);
        assertThat(testProperty.isIsLift()).isEqualTo(UPDATED_IS_LIFT);
    }

    @Test
    @Transactional
    public void updateNonExistingProperty() throws Exception {
        int databaseSizeBeforeUpdate = propertyRepository.findAll().size();

        // Create the Property
        PropertyDTO propertyDTO = propertyMapper.toDto(property);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restPropertyMockMvc.perform(put("/api/properties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(propertyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Property in the database
        List<Property> propertyList = propertyRepository.findAll();
        assertThat(propertyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProperty() throws Exception {
        // Initialize the database
        propertyRepository.saveAndFlush(property);

        int databaseSizeBeforeDelete = propertyRepository.findAll().size();

        // Get the property
        restPropertyMockMvc.perform(delete("/api/properties/{id}", property.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Property> propertyList = propertyRepository.findAll();
        assertThat(propertyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Property.class);
        Property property1 = new Property();
        property1.setId(1L);
        Property property2 = new Property();
        property2.setId(property1.getId());
        assertThat(property1).isEqualTo(property2);
        property2.setId(2L);
        assertThat(property1).isNotEqualTo(property2);
        property1.setId(null);
        assertThat(property1).isNotEqualTo(property2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PropertyDTO.class);
        PropertyDTO propertyDTO1 = new PropertyDTO();
        propertyDTO1.setId(1L);
        PropertyDTO propertyDTO2 = new PropertyDTO();
        assertThat(propertyDTO1).isNotEqualTo(propertyDTO2);
        propertyDTO2.setId(propertyDTO1.getId());
        assertThat(propertyDTO1).isEqualTo(propertyDTO2);
        propertyDTO2.setId(2L);
        assertThat(propertyDTO1).isNotEqualTo(propertyDTO2);
        propertyDTO1.setId(null);
        assertThat(propertyDTO1).isNotEqualTo(propertyDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(propertyMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(propertyMapper.fromId(null)).isNull();
    }
}
