package in.walkin.walkinhome.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A LivingRoom.
 */
@Entity
@Table(name = "living_room")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LivingRoom implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "is_sofa")
    private Boolean isSofa;

    @Column(name = "is_tv")
    private Boolean isTv;

    @Column(name = "is_wash_machine")
    private Boolean isWashMachine;

    @Column(name = "is_dish_tv")
    private Boolean isDishTv;

    @Column(name = "is_wifi")
    private Boolean isWifi;

    @Column(name = "is_ac")
    private Boolean isAc;

    @ManyToOne
    @JsonIgnoreProperties("livingRooms")
    @JsonIgnore
    private Property property;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsSofa() {
        return isSofa;
    }

    public LivingRoom isSofa(Boolean isSofa) {
        this.isSofa = isSofa;
        return this;
    }

    public void setIsSofa(Boolean isSofa) {
        this.isSofa = isSofa;
    }

    public Boolean isIsTv() {
        return isTv;
    }

    public LivingRoom isTv(Boolean isTv) {
        this.isTv = isTv;
        return this;
    }

    public void setIsTv(Boolean isTv) {
        this.isTv = isTv;
    }

    public Boolean isIsWashMachine() {
        return isWashMachine;
    }

    public LivingRoom isWashMachine(Boolean isWashMachine) {
        this.isWashMachine = isWashMachine;
        return this;
    }

    public void setIsWashMachine(Boolean isWashMachine) {
        this.isWashMachine = isWashMachine;
    }

    public Boolean isIsDishTv() {
        return isDishTv;
    }

    public LivingRoom isDishTv(Boolean isDishTv) {
        this.isDishTv = isDishTv;
        return this;
    }

    public void setIsDishTv(Boolean isDishTv) {
        this.isDishTv = isDishTv;
    }

    public Boolean isIsWifi() {
        return isWifi;
    }

    public LivingRoom isWifi(Boolean isWifi) {
        this.isWifi = isWifi;
        return this;
    }

    public void setIsWifi(Boolean isWifi) {
        this.isWifi = isWifi;
    }

    public Boolean isIsAc() {
        return isAc;
    }

    public LivingRoom isAc(Boolean isAc) {
        this.isAc = isAc;
        return this;
    }

    public void setIsAc(Boolean isAc) {
        this.isAc = isAc;
    }

    public Property getProperty() {
        return property;
    }

    public LivingRoom property(Property property) {
        this.property = property;
        return this;
    }

    public void setProperty(Property property) {
        this.property = property;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LivingRoom livingRoom = (LivingRoom) o;
        if (livingRoom.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), livingRoom.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LivingRoom{" +
            "id=" + getId() +
            ", isSofa='" + isIsSofa() + "'" +
            ", isTv='" + isIsTv() + "'" +
            ", isWashMachine='" + isIsWashMachine() + "'" +
            ", isDishTv='" + isIsDishTv() + "'" +
            ", isWifi='" + isIsWifi() + "'" +
            ", isAc='" + isIsAc() + "'" +
            "}";
    }
}
