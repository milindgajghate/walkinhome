package in.walkin.walkinhome.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A PropertyDetails.
 */
@Entity
@Table(name = "property_details")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PropertyDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "no_of_floors")
    private Integer noOfFloors;

    @Column(name = "nearby_hospital")
    private Integer nearbyHospital;

    @Column(name = "nearby_shop")
    private Integer nearbyShop;

    @Column(name = "nearby_busstop")
    private Integer nearbyBusstop;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNoOfFloors() {
        return noOfFloors;
    }

    public PropertyDetails noOfFloors(Integer noOfFloors) {
        this.noOfFloors = noOfFloors;
        return this;
    }

    public void setNoOfFloors(Integer noOfFloors) {
        this.noOfFloors = noOfFloors;
    }

    public Integer getNearbyHospital() {
        return nearbyHospital;
    }

    public PropertyDetails nearbyHospital(Integer nearbyHospital) {
        this.nearbyHospital = nearbyHospital;
        return this;
    }

    public void setNearbyHospital(Integer nearbyHospital) {
        this.nearbyHospital = nearbyHospital;
    }

    public Integer getNearbyShop() {
        return nearbyShop;
    }

    public PropertyDetails nearbyShop(Integer nearbyShop) {
        this.nearbyShop = nearbyShop;
        return this;
    }

    public void setNearbyShop(Integer nearbyShop) {
        this.nearbyShop = nearbyShop;
    }

    public Integer getNearbyBusstop() {
        return nearbyBusstop;
    }

    public PropertyDetails nearbyBusstop(Integer nearbyBusstop) {
        this.nearbyBusstop = nearbyBusstop;
        return this;
    }

    public void setNearbyBusstop(Integer nearbyBusstop) {
        this.nearbyBusstop = nearbyBusstop;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PropertyDetails propertyDetails = (PropertyDetails) o;
        if (propertyDetails.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), propertyDetails.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PropertyDetails{" +
            "id=" + getId() +
            ", noOfFloors=" + getNoOfFloors() +
            ", nearbyHospital=" + getNearbyHospital() +
            ", nearbyShop=" + getNearbyShop() +
            ", nearbyBusstop=" + getNearbyBusstop() +
            "}";
    }
}
