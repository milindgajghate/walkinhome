package in.walkin.walkinhome.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Booking.
 */
@Entity
@Table(name = "booking")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Booking implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "booking_start_date")
    private Instant bookingStartDate;

    @Column(name = "booking_end_date")
    private Instant bookingEndDate;

    @Column(name = "price")
    private Double price;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn
    private Bed bed;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn
    private Tenant tenant;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getBookingStartDate() {
        return bookingStartDate;
    }

    public Booking bookingStartDate(Instant bookingStartDate) {
        this.bookingStartDate = bookingStartDate;
        return this;
    }

    public void setBookingStartDate(Instant bookingStartDate) {
        this.bookingStartDate = bookingStartDate;
    }

    public Instant getBookingEndDate() {
        return bookingEndDate;
    }

    public Booking bookingEndDate(Instant bookingEndDate) {
        this.bookingEndDate = bookingEndDate;
        return this;
    }

    public void setBookingEndDate(Instant bookingEndDate) {
        this.bookingEndDate = bookingEndDate;
    }

    public Double getPrice() {
        return price;
    }

    public Booking price(Double price) {
        this.price = price;
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Bed getBed() {
        return bed;
    }

    public Booking bed(Bed bed) {
        this.bed = bed;
        return this;
    }

    public void setBed(Bed bed) {
        this.bed = bed;
    }

    public Tenant getTenant() {
        return tenant;
    }

    public Booking tenant(Tenant tenant) {
        this.tenant = tenant;
        return this;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Booking booking = (Booking) o;
        if (booking.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), booking.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Booking{" +
            "id=" + getId() +
            ", bookingStartDate='" + getBookingStartDate() + "'" +
            ", bookingEndDate='" + getBookingEndDate() + "'" +
            ", price=" + getPrice() +
            "}";
    }
}
