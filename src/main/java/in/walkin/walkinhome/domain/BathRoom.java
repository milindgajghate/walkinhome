package in.walkin.walkinhome.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A BathRoom.
 */
@Entity
@Table(name = "bath_room")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BathRoom implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "is_attached_type")
    private Boolean isAttachedType;

    @Column(name = "is_western_toilet")
    private Boolean isWesternToilet;

    @Column(name = "is_geyser")
    private Boolean isGeyser;

    @Column(name = "is_full_day_water")
    private Boolean isFullDayWater;

    @ManyToOne
    @JsonIgnoreProperties("bathRooms")
    @JsonIgnore
    private Property property;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsAttachedType() {
        return isAttachedType;
    }

    public BathRoom isAttachedType(Boolean isAttachedType) {
        this.isAttachedType = isAttachedType;
        return this;
    }

    public void setIsAttachedType(Boolean isAttachedType) {
        this.isAttachedType = isAttachedType;
    }

    public Boolean isIsWesternToilet() {
        return isWesternToilet;
    }

    public BathRoom isWesternToilet(Boolean isWesternToilet) {
        this.isWesternToilet = isWesternToilet;
        return this;
    }

    public void setIsWesternToilet(Boolean isWesternToilet) {
        this.isWesternToilet = isWesternToilet;
    }

    public Boolean isIsGeyser() {
        return isGeyser;
    }

    public BathRoom isGeyser(Boolean isGeyser) {
        this.isGeyser = isGeyser;
        return this;
    }

    public void setIsGeyser(Boolean isGeyser) {
        this.isGeyser = isGeyser;
    }

    public Boolean isIsFullDayWater() {
        return isFullDayWater;
    }

    public BathRoom isFullDayWater(Boolean isFullDayWater) {
        this.isFullDayWater = isFullDayWater;
        return this;
    }

    public void setIsFullDayWater(Boolean isFullDayWater) {
        this.isFullDayWater = isFullDayWater;
    }

    public Property getProperty() {
        return property;
    }

    public BathRoom property(Property property) {
        this.property = property;
        return this;
    }

    public void setProperty(Property property) {
        this.property = property;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BathRoom bathRoom = (BathRoom) o;
        if (bathRoom.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bathRoom.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BathRoom{" +
            "id=" + getId() +
            ", isAttachedType='" + isIsAttachedType() + "'" +
            ", isWesternToilet='" + isIsWesternToilet() + "'" +
            ", isGeyser='" + isIsGeyser() + "'" +
            ", isFullDayWater='" + isIsFullDayWater() + "'" +
            "}";
    }
}
