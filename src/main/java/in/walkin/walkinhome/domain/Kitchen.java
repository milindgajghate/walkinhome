package in.walkin.walkinhome.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Kitchen.
 */
@Entity
@Table(name = "kitchen")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Kitchen implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "is_dining")
    private Boolean isDining;

    @Column(name = "is_fridge")
    private Boolean isFridge;

    @Column(name = "is_gas_stove")
    private Boolean isGasStove;

    @Column(name = "is_modular")
    private Boolean isModular;

    @Column(name = "is_induction")
    private Boolean isInduction;

    @Column(name = "is_microvave")
    private Boolean isMicrovave;

    @Column(name = "is_water_filter")
    private Boolean isWaterFilter;

    @Column(name = "is_crockery")
    private Boolean isCrockery;

    @Column(name = "is_toaster")
    private Boolean isToaster;

    @Column(name = "is_grinder")
    private Boolean isGrinder;

    @Column(name = "is_rice_cooker")
    private Boolean isRiceCooker;

    @Column(name = "is_lpg")
    private Boolean isLpg;

    @ManyToOne
    @JsonIgnoreProperties("kitchens")
    @JsonIgnore
    private Property property;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsDining() {
        return isDining;
    }

    public Kitchen isDining(Boolean isDining) {
        this.isDining = isDining;
        return this;
    }

    public void setIsDining(Boolean isDining) {
        this.isDining = isDining;
    }

    public Boolean isIsFridge() {
        return isFridge;
    }

    public Kitchen isFridge(Boolean isFridge) {
        this.isFridge = isFridge;
        return this;
    }

    public void setIsFridge(Boolean isFridge) {
        this.isFridge = isFridge;
    }

    public Boolean isIsGasStove() {
        return isGasStove;
    }

    public Kitchen isGasStove(Boolean isGasStove) {
        this.isGasStove = isGasStove;
        return this;
    }

    public void setIsGasStove(Boolean isGasStove) {
        this.isGasStove = isGasStove;
    }

    public Boolean isIsModular() {
        return isModular;
    }

    public Kitchen isModular(Boolean isModular) {
        this.isModular = isModular;
        return this;
    }

    public void setIsModular(Boolean isModular) {
        this.isModular = isModular;
    }

    public Boolean isIsInduction() {
        return isInduction;
    }

    public Kitchen isInduction(Boolean isInduction) {
        this.isInduction = isInduction;
        return this;
    }

    public void setIsInduction(Boolean isInduction) {
        this.isInduction = isInduction;
    }

    public Boolean isIsMicrovave() {
        return isMicrovave;
    }

    public Kitchen isMicrovave(Boolean isMicrovave) {
        this.isMicrovave = isMicrovave;
        return this;
    }

    public void setIsMicrovave(Boolean isMicrovave) {
        this.isMicrovave = isMicrovave;
    }

    public Boolean isIsWaterFilter() {
        return isWaterFilter;
    }

    public Kitchen isWaterFilter(Boolean isWaterFilter) {
        this.isWaterFilter = isWaterFilter;
        return this;
    }

    public void setIsWaterFilter(Boolean isWaterFilter) {
        this.isWaterFilter = isWaterFilter;
    }

    public Boolean isIsCrockery() {
        return isCrockery;
    }

    public Kitchen isCrockery(Boolean isCrockery) {
        this.isCrockery = isCrockery;
        return this;
    }

    public void setIsCrockery(Boolean isCrockery) {
        this.isCrockery = isCrockery;
    }

    public Boolean isIsToaster() {
        return isToaster;
    }

    public Kitchen isToaster(Boolean isToaster) {
        this.isToaster = isToaster;
        return this;
    }

    public void setIsToaster(Boolean isToaster) {
        this.isToaster = isToaster;
    }

    public Boolean isIsGrinder() {
        return isGrinder;
    }

    public Kitchen isGrinder(Boolean isGrinder) {
        this.isGrinder = isGrinder;
        return this;
    }

    public void setIsGrinder(Boolean isGrinder) {
        this.isGrinder = isGrinder;
    }

    public Boolean isIsRiceCooker() {
        return isRiceCooker;
    }

    public Kitchen isRiceCooker(Boolean isRiceCooker) {
        this.isRiceCooker = isRiceCooker;
        return this;
    }

    public void setIsRiceCooker(Boolean isRiceCooker) {
        this.isRiceCooker = isRiceCooker;
    }

    public Boolean isIsLpg() {
        return isLpg;
    }

    public Kitchen isLpg(Boolean isLpg) {
        this.isLpg = isLpg;
        return this;
    }

    public void setIsLpg(Boolean isLpg) {
        this.isLpg = isLpg;
    }

    public Property getProperty() {
        return property;
    }

    public Kitchen property(Property property) {
        this.property = property;
        return this;
    }

    public void setProperty(Property property) {
        this.property = property;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Kitchen kitchen = (Kitchen) o;
        if (kitchen.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), kitchen.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Kitchen{" +
            "id=" + getId() +
            ", isDining='" + isIsDining() + "'" +
            ", isFridge='" + isIsFridge() + "'" +
            ", isGasStove='" + isIsGasStove() + "'" +
            ", isModular='" + isIsModular() + "'" +
            ", isInduction='" + isIsInduction() + "'" +
            ", isMicrovave='" + isIsMicrovave() + "'" +
            ", isWaterFilter='" + isIsWaterFilter() + "'" +
            ", isCrockery='" + isIsCrockery() + "'" +
            ", isToaster='" + isIsToaster() + "'" +
            ", isGrinder='" + isIsGrinder() + "'" +
            ", isRiceCooker='" + isIsRiceCooker() + "'" +
            ", isLpg='" + isIsLpg() + "'" +
            "}";
    }
}
