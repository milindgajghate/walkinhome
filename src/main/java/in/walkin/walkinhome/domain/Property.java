package in.walkin.walkinhome.domain;

import in.walkin.walkinhome.domain.enumeration.FoodPref;
import in.walkin.walkinhome.domain.enumeration.Status;
import in.walkin.walkinhome.domain.enumeration.TenantType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;

/**
 * A Property.
 */
@Entity
@Table(name = "property")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Property implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Column(name = "area")
    private Float area;

    @Column(name = "jhi_desc")
    private String desc;

    @Column(name = "car_parking_no")
    private Integer carParkingNo;

    @Column(name = "two_wheeler_parking_no")
    private Integer twoWheelerParkingNo;

    @Column(name = "price")
    private Double price;

    @Column(name = "built_year")
    private Integer builtYear;

    @Column(name = "floor")
    private Integer floor;

    @Enumerated(EnumType.STRING)
    @Column(name = "tenant_type")
    private TenantType tenantType;

    @Enumerated(EnumType.STRING)
    @Column(name = "food_pref")
    private FoodPref foodPref;

    @Column(name = "is_security")
    private Boolean isSecurity;

    @Column(name = "is_lift")
    private Boolean isLift;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn
    private Discount discount;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn
    private Address address;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(unique = true)
    private PropertyDetails propertyDetails;

    @OneToMany(mappedBy = "property", cascade=ALL)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<BathRoom> bathRooms = new HashSet<>();

    @OneToMany(mappedBy = "property", cascade=ALL)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<BedRoom> bedRooms = new HashSet<>();

    @OneToMany(mappedBy = "property", cascade=ALL)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Kitchen> kitchens = new HashSet<>();

    @OneToMany(mappedBy = "property", cascade=ALL)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<LivingRoom> livingRooms = new HashSet<>();

    @OneToMany(mappedBy = "property", cascade=ALL)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Image> images = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public Property status(Status status) {
        this.status = status;
        return this;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Float getArea() {
        return area;
    }

    public Property area(Float area) {
        this.area = area;
        return this;
    }

    public void setArea(Float area) {
        this.area = area;
    }

    public String getDesc() {
        return desc;
    }

    public Property desc(String desc) {
        this.desc = desc;
        return this;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getCarParkingNo() {
        return carParkingNo;
    }

    public Property carParkingNo(Integer carParkingNo) {
        this.carParkingNo = carParkingNo;
        return this;
    }

    public void setCarParkingNo(Integer carParkingNo) {
        this.carParkingNo = carParkingNo;
    }

    public Integer getTwoWheelerParkingNo() {
        return twoWheelerParkingNo;
    }

    public Property twoWheelerParkingNo(Integer twoWheelerParkingNo) {
        this.twoWheelerParkingNo = twoWheelerParkingNo;
        return this;
    }

    public void setTwoWheelerParkingNo(Integer twoWheelerParkingNo) {
        this.twoWheelerParkingNo = twoWheelerParkingNo;
    }

    public Double getPrice() {
        return price;
    }

    public Property price(Double price) {
        this.price = price;
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getBuiltYear() {
        return builtYear;
    }

    public Property builtYear(Integer builtYear) {
        this.builtYear = builtYear;
        return this;
    }

    public void setBuiltYear(Integer builtYear) {
        this.builtYear = builtYear;
    }

    public Integer getFloor() {
        return floor;
    }

    public Property floor(Integer floor) {
        this.floor = floor;
        return this;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public TenantType getTenantType() {
        return tenantType;
    }

    public Property tenantType(TenantType tenantType) {
        this.tenantType = tenantType;
        return this;
    }

    public void setTenantType(TenantType tenantType) {
        this.tenantType = tenantType;
    }

    public FoodPref getFoodPref() {
        return foodPref;
    }

    public Property foodPref(FoodPref foodPref) {
        this.foodPref = foodPref;
        return this;
    }

    public void setFoodPref(FoodPref foodPref) {
        this.foodPref = foodPref;
    }

    public Boolean isIsSecurity() {
        return isSecurity;
    }

    public Property isSecurity(Boolean isSecurity) {
        this.isSecurity = isSecurity;
        return this;
    }

    public void setIsSecurity(Boolean isSecurity) {
        this.isSecurity = isSecurity;
    }

    public Boolean isIsLift() {
        return isLift;
    }

    public Property isLift(Boolean isLift) {
        this.isLift = isLift;
        return this;
    }

    public void setIsLift(Boolean isLift) {
        this.isLift = isLift;
    }

    public Discount getDiscount() {
        return discount;
    }

    public Property discount(Discount discount) {
        this.discount = discount;
        return this;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public Address getAddress() {
        return address;
    }

    public Property address(Address address) {
        this.address = address;
        return this;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public PropertyDetails getPropertyDetails() {
        return propertyDetails;
    }

    public Property propertyDetails(PropertyDetails propertyDetails) {
        this.propertyDetails = propertyDetails;
        return this;
    }

    public void setPropertyDetails(PropertyDetails propertyDetails) {
        this.propertyDetails = propertyDetails;
    }

    public Set<BathRoom> getBathRooms() {
        return bathRooms;
    }

    public Property bathRooms(Set<BathRoom> bathRooms) {
        this.bathRooms = bathRooms;
        return this;
    }

    public Property addBathRoom(BathRoom bathRoom) {
        this.bathRooms.add(bathRoom);
        return this;
    }

    public Property removeBathRoom(BathRoom bathRoom) {
        this.bathRooms.remove(bathRoom);
        return this;
    }

    public void setBathRooms(Set<BathRoom> bathRooms) {
        this.bathRooms = bathRooms;
    }

    public Set<BedRoom> getBedRooms() {
        return bedRooms;
    }

    public Property bedRooms(Set<BedRoom> bedRooms) {
        this.bedRooms = bedRooms;
        return this;
    }

    public Property addBedRoom(BedRoom bedRoom) {
        this.bedRooms.add(bedRoom);
        return this;
    }

    public Property removeBedRoom(BedRoom bedRoom) {
        this.bedRooms.remove(bedRoom);
        return this;
    }

    public void setBedRooms(Set<BedRoom> bedRooms) {
        this.bedRooms = bedRooms;
    }

    public Set<Kitchen> getKitchens() {
        return kitchens;
    }

    public Property kitchens(Set<Kitchen> kitchens) {
        this.kitchens = kitchens;
        return this;
    }

    public Property addKitchen(Kitchen kitchen) {
        this.kitchens.add(kitchen);
        return this;
    }

    public Property removeKitchen(Kitchen kitchen) {
        this.kitchens.remove(kitchen);
        return this;
    }

    public void setKitchens(Set<Kitchen> kitchens) {
        this.kitchens = kitchens;
    }

    public Set<LivingRoom> getLivingRooms() {
        return livingRooms;
    }

    public Property livingRooms(Set<LivingRoom> livingRooms) {
        this.livingRooms = livingRooms;
        return this;
    }

    public Property addLivingRoom(LivingRoom livingRoom) {
        this.livingRooms.add(livingRoom);
        return this;
    }

    public Property removeLivingRoom(LivingRoom livingRoom) {
        this.livingRooms.remove(livingRoom);
        return this;
    }

    public void setLivingRooms(Set<LivingRoom> livingRooms) {
        this.livingRooms = livingRooms;
    }

    public Set<Image> getImages() {
        return images;
    }

    public Property images(Set<Image> images) {
        this.images = images;
        return this;
    }

    public Property addImage(Image image) {
        this.images.add(image);
        return this;
    }

    public Property removeImage(Image image) {
        this.images.remove(image);
        return this;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Property property = (Property) o;
        if (property.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), property.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Property{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", area=" + getArea() +
            ", desc='" + getDesc() + "'" +
            ", carParkingNo=" + getCarParkingNo() +
            ", twoWheelerParkingNo=" + getTwoWheelerParkingNo() +
            ", price=" + getPrice() +
            ", builtYear=" + getBuiltYear() +
            ", floor=" + getFloor() +
            ", tenantType='" + getTenantType() + "'" +
            ", foodPref='" + getFoodPref() + "'" +
            ", isSecurity='" + isIsSecurity() + "'" +
            ", isLift='" + isIsLift() + "'" +
            "}";
    }
}
