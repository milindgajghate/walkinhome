package in.walkin.walkinhome.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import in.walkin.walkinhome.domain.enumeration.Status;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;

/**
 * A BedRoom.
 */
@Entity
@Table(name = "bed_room")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BedRoom implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Column(name = "is_study_table")
    private Boolean isStudyTable;

    @Column(name = "is_cup_board")
    private Boolean isCupBoard;

    @Column(name = "is_terrace")
    private Boolean isTerrace;

    @Column(name = "is_ac_boolean")
    private Boolean isAcBoolean;

    @Column(name = "price")
    private Double price;

    @OneToOne(cascade=ALL)
    @JoinColumn
    private Discount discount;

    @OneToMany(cascade=ALL, mappedBy="bedRoom")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Bed> bedIds = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("bedRooms")
    @JsonIgnore
    private Property property;


    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public BedRoom status(Status status) {
        this.status = status;
        return this;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Boolean isIsStudyTable() {
        return isStudyTable;
    }

    public BedRoom isStudyTable(Boolean isStudyTable) {
        this.isStudyTable = isStudyTable;
        return this;
    }

    public void setIsStudyTable(Boolean isStudyTable) {
        this.isStudyTable = isStudyTable;
    }

    public Boolean isIsCupBoard() {
        return isCupBoard;
    }

    public BedRoom isCupBoard(Boolean isCupBoard) {
        this.isCupBoard = isCupBoard;
        return this;
    }

    public void setIsCupBoard(Boolean isCupBoard) {
        this.isCupBoard = isCupBoard;
    }

    public Boolean isIsTerrace() {
        return isTerrace;
    }

    public BedRoom isTerrace(Boolean isTerrace) {
        this.isTerrace = isTerrace;
        return this;
    }

    public void setIsTerrace(Boolean isTerrace) {
        this.isTerrace = isTerrace;
    }

    public Boolean isIsAcBoolean() {
        return isAcBoolean;
    }

    public BedRoom isAcBoolean(Boolean isAcBoolean) {
        this.isAcBoolean = isAcBoolean;
        return this;
    }

    public void setIsAcBoolean(Boolean isAcBoolean) {
        this.isAcBoolean = isAcBoolean;
    }

    public Double getPrice() {
        return price;
    }

    public BedRoom price(Double price) {
        this.price = price;
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Discount getDiscount() {
        return discount;
    }

    public BedRoom discount(Discount discount) {
        this.discount = discount;
        return this;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public Set<Bed> getBedIds() {
        return bedIds;
    }

    public BedRoom bedIds(Set<Bed> beds) {
        this.bedIds = beds;
        return this;
    }

    public BedRoom addBedId(Bed bed) {
        this.bedIds.add(bed);
        return this;
    }

    public BedRoom removeBedId(Bed bed) {
        this.bedIds.remove(bed);
        return this;
    }

    public void setBedIds(Set<Bed> beds) {
        this.bedIds = beds;
    }

    public Property getProperty() {
        return property;
    }

    public BedRoom property(Property property) {
        this.property = property;
        return this;
    }

    public void setProperty(Property property) {
        this.property = property;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BedRoom bedRoom = (BedRoom) o;
        if (bedRoom.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bedRoom.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BedRoom{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", isStudyTable='" + isIsStudyTable() + "'" +
            ", isCupBoard='" + isIsCupBoard() + "'" +
            ", isTerrace='" + isIsTerrace() + "'" +
            ", isAcBoolean='" + isIsAcBoolean() + "'" +
            ", price=" + getPrice() +
            "}";
    }
}
