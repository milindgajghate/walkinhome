package in.walkin.walkinhome.domain.enumeration;

/**
 * The TenantType enumeration.
 */
public enum TenantType {
    GIRL, BOY, FAMILY
}
