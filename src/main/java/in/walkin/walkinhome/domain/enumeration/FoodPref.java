package in.walkin.walkinhome.domain.enumeration;

/**
 * The FoodPref enumeration.
 */
public enum FoodPref {
    VEG, NON_VEG
}
