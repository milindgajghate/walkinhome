package in.walkin.walkinhome.domain.enumeration;

/**
 * The Status enumeration.
 */
public enum Status {
    BOOKED, AVAILABLE
}
