package in.walkin.walkinhome.domain.enumeration;

/**
 * The RoomType enumeration.
 */
public enum RoomType {
    KITCHEN, BEDROOM, BATHROOM
}
