package in.walkin.walkinhome.service.dto;

import in.walkin.walkinhome.domain.Address;
import in.walkin.walkinhome.domain.Discount;
import in.walkin.walkinhome.domain.PropertyDetails;
import in.walkin.walkinhome.domain.enumeration.FoodPref;
import in.walkin.walkinhome.domain.enumeration.Status;
import in.walkin.walkinhome.domain.enumeration.TenantType;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the Property entity.
 */
public class PropertyDTO implements Serializable {

    private Long id;

    private Status status;

    private Float area;

    private String desc;

    private Integer carParkingNo;

    private Integer twoWheelerParkingNo;

    private Double price;

    private Integer builtYear;

    private Integer floor;

    private TenantType tenantType;

    private FoodPref foodPref;

    private Boolean isSecurity;

    private Boolean isLift;

    private Discount discount;

    private Address address;

    private PropertyDetails propertyDetails;

    private Set<BathRoomDTO> bathRooms = new HashSet<>();

    private Set<BedRoomDTO> bedRooms = new HashSet<>();

    private Set<KitchenDTO> kitchens = new HashSet<>();

    private Set<LivingRoomDTO> livingRooms = new HashSet<>();

    private Set<ImageDTO> images = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Float getArea() {
        return area;
    }

    public void setArea(Float area) {
        this.area = area;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getCarParkingNo() {
        return carParkingNo;
    }

    public void setCarParkingNo(Integer carParkingNo) {
        this.carParkingNo = carParkingNo;
    }

    public Integer getTwoWheelerParkingNo() {
        return twoWheelerParkingNo;
    }

    public void setTwoWheelerParkingNo(Integer twoWheelerParkingNo) {
        this.twoWheelerParkingNo = twoWheelerParkingNo;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getBuiltYear() {
        return builtYear;
    }

    public void setBuiltYear(Integer builtYear) {
        this.builtYear = builtYear;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public TenantType getTenantType() {
        return tenantType;
    }

    public void setTenantType(TenantType tenantType) {
        this.tenantType = tenantType;
    }

    public FoodPref getFoodPref() {
        return foodPref;
    }

    public void setFoodPref(FoodPref foodPref) {
        this.foodPref = foodPref;
    }

    public Boolean isIsSecurity() {
        return isSecurity;
    }

    public void setIsSecurity(Boolean isSecurity) {
        this.isSecurity = isSecurity;
    }

    public Boolean isIsLift() {
        return isLift;
    }

    public void setIsLift(Boolean isLift) {
        this.isLift = isLift;
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public PropertyDetails getPropertyDetails() {
        return propertyDetails;
    }

    public void setPropertyDetails(PropertyDetails propertyDetails) {
        this.propertyDetails = propertyDetails;
    }

    public Set<BathRoomDTO> getBathRooms() {
        return bathRooms;
    }

    public void setBathRooms(Set<BathRoomDTO> bathRooms) {
        this.bathRooms = bathRooms;
    }

    public Set<BedRoomDTO> getBedRooms() {
        return bedRooms;
    }

    public void setBedRooms(Set<BedRoomDTO> bedRooms) {
        this.bedRooms = bedRooms;
    }

    public Set<KitchenDTO> getKitchens() {
        return kitchens;
    }

    public void setKitchens(Set<KitchenDTO> kitchens) {
        this.kitchens = kitchens;
    }

    public Set<LivingRoomDTO> getLivingRooms() {
        return livingRooms;
    }

    public void setLivingRooms(Set<LivingRoomDTO> livingRooms) {
        this.livingRooms = livingRooms;
    }

    public Set<ImageDTO> getImages() {
        return images;
    }

    public void setImages(Set<ImageDTO> images) {
        this.images = images;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PropertyDTO propertyDTO = (PropertyDTO) o;
        if (propertyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), propertyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PropertyDTO{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", area=" + getArea() +
            ", desc='" + getDesc() + "'" +
            ", carParkingNo=" + getCarParkingNo() +
            ", twoWheelerParkingNo=" + getTwoWheelerParkingNo() +
            ", price=" + getPrice() +
            ", builtYear=" + getBuiltYear() +
            ", floor=" + getFloor() +
            ", tenantType='" + getTenantType() + "'" +
            ", foodPref='" + getFoodPref() + "'" +
            ", isSecurity='" + isIsSecurity() + "'" +
            ", isLift='" + isIsLift() + "'" +
            "}";
    }
}
