package in.walkin.walkinhome.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the BathRoom entity.
 */
public class BathRoomDTO implements Serializable {

    private Long id;

    private Boolean isAttachedType;

    private Boolean isWesternToilet;

    private Boolean isGeyser;

    private Boolean isFullDayWater;

    private Long propertyId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsAttachedType() {
        return isAttachedType;
    }

    public void setIsAttachedType(Boolean isAttachedType) {
        this.isAttachedType = isAttachedType;
    }

    public Boolean isIsWesternToilet() {
        return isWesternToilet;
    }

    public void setIsWesternToilet(Boolean isWesternToilet) {
        this.isWesternToilet = isWesternToilet;
    }

    public Boolean isIsGeyser() {
        return isGeyser;
    }

    public void setIsGeyser(Boolean isGeyser) {
        this.isGeyser = isGeyser;
    }

    public Boolean isIsFullDayWater() {
        return isFullDayWater;
    }

    public void setIsFullDayWater(Boolean isFullDayWater) {
        this.isFullDayWater = isFullDayWater;
    }

    public Long getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Long propertyId) {
        this.propertyId = propertyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BathRoomDTO bathRoomDTO = (BathRoomDTO) o;
        if (bathRoomDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bathRoomDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BathRoomDTO{" +
            "id=" + getId() +
            ", isAttachedType='" + isIsAttachedType() + "'" +
            ", isWesternToilet='" + isIsWesternToilet() + "'" +
            ", isGeyser='" + isIsGeyser() + "'" +
            ", isFullDayWater='" + isIsFullDayWater() + "'" +
            ", property=" + getPropertyId() +
            "}";
    }
}
