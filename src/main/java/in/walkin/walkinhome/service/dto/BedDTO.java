package in.walkin.walkinhome.service.dto;

import in.walkin.walkinhome.domain.Discount;
import in.walkin.walkinhome.domain.enumeration.Status;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Bed entity.
 */
public class BedDTO implements Serializable {

    private Long id;

    private Status status;

    private Double price;

    private Discount discount;

    private Long bedRoom;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public Long getBedRoom() {
        return bedRoom;
    }

    public void setBedRoom(Long bedRoom) {
        this.bedRoom = bedRoom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BedDTO bedDTO = (BedDTO) o;
        if (bedDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bedDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BedDTO{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", price=" + getPrice() +
            ", discount=" + getDiscount().toString() +
            ", bedRoom=" + getBedRoom().toString() +
            "}";
    }
}
