package in.walkin.walkinhome.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the PropertyDetails entity.
 */
public class PropertyDetailsDTO implements Serializable {

    private Long id;

    private Integer noOfFloors;

    private Integer nearbyHospital;

    private Integer nearbyShop;

    private Integer nearbyBusstop;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNoOfFloors() {
        return noOfFloors;
    }

    public void setNoOfFloors(Integer noOfFloors) {
        this.noOfFloors = noOfFloors;
    }

    public Integer getNearbyHospital() {
        return nearbyHospital;
    }

    public void setNearbyHospital(Integer nearbyHospital) {
        this.nearbyHospital = nearbyHospital;
    }

    public Integer getNearbyShop() {
        return nearbyShop;
    }

    public void setNearbyShop(Integer nearbyShop) {
        this.nearbyShop = nearbyShop;
    }

    public Integer getNearbyBusstop() {
        return nearbyBusstop;
    }

    public void setNearbyBusstop(Integer nearbyBusstop) {
        this.nearbyBusstop = nearbyBusstop;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PropertyDetailsDTO propertyDetailsDTO = (PropertyDetailsDTO) o;
        if (propertyDetailsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), propertyDetailsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PropertyDetailsDTO{" +
            "id=" + getId() +
            ", noOfFloors=" + getNoOfFloors() +
            ", nearbyHospital=" + getNearbyHospital() +
            ", nearbyShop=" + getNearbyShop() +
            ", nearbyBusstop=" + getNearbyBusstop() +
            "}";
    }
}
