package in.walkin.walkinhome.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the Booking entity.
 */
public class BookingDTO implements Serializable {

    private Long id;

    private Instant bookingStartDate;

    private Instant bookingEndDate;

    private Double price;

    private Long bedId;

    private Long tenantId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getBookingStartDate() {
        return bookingStartDate;
    }

    public void setBookingStartDate(Instant bookingStartDate) {
        this.bookingStartDate = bookingStartDate;
    }

    public Instant getBookingEndDate() {
        return bookingEndDate;
    }

    public void setBookingEndDate(Instant bookingEndDate) {
        this.bookingEndDate = bookingEndDate;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getBedId() {
        return bedId;
    }

    public void setBedId(Long bedId) {
        this.bedId = bedId;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BookingDTO bookingDTO = (BookingDTO) o;
        if (bookingDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bookingDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BookingDTO{" +
            "id=" + getId() +
            ", bookingStartDate='" + getBookingStartDate() + "'" +
            ", bookingEndDate='" + getBookingEndDate() + "'" +
            ", price=" + getPrice() +
            ", bed=" + getBedId() +
            ", tenant=" + getTenantId() +
            "}";
    }
}
