package in.walkin.walkinhome.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the LivingRoom entity.
 */
public class LivingRoomDTO implements Serializable {

    private Long id;

    private Boolean isSofa;

    private Boolean isTv;

    private Boolean isWashMachine;

    private Boolean isDishTv;

    private Boolean isWifi;

    private Boolean isAc;

    private Long propertyId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsSofa() {
        return isSofa;
    }

    public void setIsSofa(Boolean isSofa) {
        this.isSofa = isSofa;
    }

    public Boolean isIsTv() {
        return isTv;
    }

    public void setIsTv(Boolean isTv) {
        this.isTv = isTv;
    }

    public Boolean isIsWashMachine() {
        return isWashMachine;
    }

    public void setIsWashMachine(Boolean isWashMachine) {
        this.isWashMachine = isWashMachine;
    }

    public Boolean isIsDishTv() {
        return isDishTv;
    }

    public void setIsDishTv(Boolean isDishTv) {
        this.isDishTv = isDishTv;
    }

    public Boolean isIsWifi() {
        return isWifi;
    }

    public void setIsWifi(Boolean isWifi) {
        this.isWifi = isWifi;
    }

    public Boolean isIsAc() {
        return isAc;
    }

    public void setIsAc(Boolean isAc) {
        this.isAc = isAc;
    }

    public Long getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Long propertyId) {
        this.propertyId = propertyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LivingRoomDTO livingRoomDTO = (LivingRoomDTO) o;
        if (livingRoomDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), livingRoomDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LivingRoomDTO{" +
            "id=" + getId() +
            ", isSofa='" + isIsSofa() + "'" +
            ", isTv='" + isIsTv() + "'" +
            ", isWashMachine='" + isIsWashMachine() + "'" +
            ", isDishTv='" + isIsDishTv() + "'" +
            ", isWifi='" + isIsWifi() + "'" +
            ", isAc='" + isIsAc() + "'" +
            ", property=" + getPropertyId() +
            "}";
    }
}
