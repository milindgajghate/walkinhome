package in.walkin.walkinhome.service.dto;

import in.walkin.walkinhome.domain.Discount;
import in.walkin.walkinhome.domain.enumeration.Status;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the BedRoom entity.
 */
public class BedRoomDTO implements Serializable {

    private Long id;

    private Status status;

    private Boolean isStudyTable;

    private Boolean isCupBoard;

    private Boolean isTerrace;

    private Boolean isAcBoolean;

    private Double price;

    private Discount discount;

    private Long propertyId;

    private Set<BedDTO> beds = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Boolean isIsStudyTable() {
        return isStudyTable;
    }

    public void setIsStudyTable(Boolean isStudyTable) {
        this.isStudyTable = isStudyTable;
    }

    public Boolean isIsCupBoard() {
        return isCupBoard;
    }

    public void setIsCupBoard(Boolean isCupBoard) {
        this.isCupBoard = isCupBoard;
    }

    public Boolean isIsTerrace() {
        return isTerrace;
    }

    public void setIsTerrace(Boolean isTerrace) {
        this.isTerrace = isTerrace;
    }

    public Boolean isIsAcBoolean() {
        return isAcBoolean;
    }

    public void setIsAcBoolean(Boolean isAcBoolean) {
        this.isAcBoolean = isAcBoolean;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public Long getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Long propertyId) {
        this.propertyId = propertyId;
    }

    public Set<BedDTO> getBeds() {
        return beds;
    }

    public void setBeds(Set<BedDTO> beds) {
        this.beds = beds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BedRoomDTO bedRoomDTO = (BedRoomDTO) o;
        if (bedRoomDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bedRoomDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BedRoomDTO{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", isStudyTable='" + isIsStudyTable() + "'" +
            ", isCupBoard='" + isIsCupBoard() + "'" +
            ", isTerrace='" + isIsTerrace() + "'" +
            ", isAcBoolean='" + isIsAcBoolean() + "'" +
            ", price=" + getPrice() +
            ", property=" + getPropertyId() +
            "}";
    }
}
