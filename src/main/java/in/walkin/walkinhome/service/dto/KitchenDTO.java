package in.walkin.walkinhome.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Kitchen entity.
 */
public class KitchenDTO implements Serializable {

    private Long id;

    private Boolean isDining;

    private Boolean isFridge;

    private Boolean isGasStove;

    private Boolean isModular;

    private Boolean isInduction;

    private Boolean isMicrovave;

    private Boolean isWaterFilter;

    private Boolean isCrockery;

    private Boolean isToaster;

    private Boolean isGrinder;

    private Boolean isRiceCooker;

    private Boolean isLpg;

    private Long propertyId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsDining() {
        return isDining;
    }

    public void setIsDining(Boolean isDining) {
        this.isDining = isDining;
    }

    public Boolean isIsFridge() {
        return isFridge;
    }

    public void setIsFridge(Boolean isFridge) {
        this.isFridge = isFridge;
    }

    public Boolean isIsGasStove() {
        return isGasStove;
    }

    public void setIsGasStove(Boolean isGasStove) {
        this.isGasStove = isGasStove;
    }

    public Boolean isIsModular() {
        return isModular;
    }

    public void setIsModular(Boolean isModular) {
        this.isModular = isModular;
    }

    public Boolean isIsInduction() {
        return isInduction;
    }

    public void setIsInduction(Boolean isInduction) {
        this.isInduction = isInduction;
    }

    public Boolean isIsMicrovave() {
        return isMicrovave;
    }

    public void setIsMicrovave(Boolean isMicrovave) {
        this.isMicrovave = isMicrovave;
    }

    public Boolean isIsWaterFilter() {
        return isWaterFilter;
    }

    public void setIsWaterFilter(Boolean isWaterFilter) {
        this.isWaterFilter = isWaterFilter;
    }

    public Boolean isIsCrockery() {
        return isCrockery;
    }

    public void setIsCrockery(Boolean isCrockery) {
        this.isCrockery = isCrockery;
    }

    public Boolean isIsToaster() {
        return isToaster;
    }

    public void setIsToaster(Boolean isToaster) {
        this.isToaster = isToaster;
    }

    public Boolean isIsGrinder() {
        return isGrinder;
    }

    public void setIsGrinder(Boolean isGrinder) {
        this.isGrinder = isGrinder;
    }

    public Boolean isIsRiceCooker() {
        return isRiceCooker;
    }

    public void setIsRiceCooker(Boolean isRiceCooker) {
        this.isRiceCooker = isRiceCooker;
    }

    public Boolean isIsLpg() {
        return isLpg;
    }

    public void setIsLpg(Boolean isLpg) {
        this.isLpg = isLpg;
    }

    public Long getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Long propertyId) {
        this.propertyId = propertyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        KitchenDTO kitchenDTO = (KitchenDTO) o;
        if (kitchenDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), kitchenDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "KitchenDTO{" +
            "id=" + getId() +
            ", isDining='" + isIsDining() + "'" +
            ", isFridge='" + isIsFridge() + "'" +
            ", isGasStove='" + isIsGasStove() + "'" +
            ", isModular='" + isIsModular() + "'" +
            ", isInduction='" + isIsInduction() + "'" +
            ", isMicrovave='" + isIsMicrovave() + "'" +
            ", isWaterFilter='" + isIsWaterFilter() + "'" +
            ", isCrockery='" + isIsCrockery() + "'" +
            ", isToaster='" + isIsToaster() + "'" +
            ", isGrinder='" + isIsGrinder() + "'" +
            ", isRiceCooker='" + isIsRiceCooker() + "'" +
            ", isLpg='" + isIsLpg() + "'" +
            ", property=" + getPropertyId() +
            "}";
    }
}
