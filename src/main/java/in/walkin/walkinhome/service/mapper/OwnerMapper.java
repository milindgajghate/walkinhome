package in.walkin.walkinhome.service.mapper;

import in.walkin.walkinhome.domain.*;
import in.walkin.walkinhome.service.dto.OwnerDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Owner and its DTO OwnerDTO.
 */
@Mapper(componentModel = "spring", uses = {AddressMapper.class})
public interface OwnerMapper extends EntityMapper<OwnerDTO, Owner> {

    @Mapping(source = "permAddress.id", target = "permAddressId")
    @Mapping(source = "tempAddress.id", target = "tempAddressId")
    OwnerDTO toDto(Owner owner);

    @Mapping(source = "permAddressId", target = "permAddress")
    @Mapping(source = "tempAddressId", target = "tempAddress")
    Owner toEntity(OwnerDTO ownerDTO);

    default Owner fromId(Long id) {
        if (id == null) {
            return null;
        }
        Owner owner = new Owner();
        owner.setId(id);
        return owner;
    }
}
