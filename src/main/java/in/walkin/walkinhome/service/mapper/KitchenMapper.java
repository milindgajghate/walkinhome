package in.walkin.walkinhome.service.mapper;

import in.walkin.walkinhome.domain.*;
import in.walkin.walkinhome.service.dto.KitchenDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Kitchen and its DTO KitchenDTO.
 */
@Mapper(componentModel = "spring", uses = {PropertyMapper.class})
public interface KitchenMapper extends EntityMapper<KitchenDTO, Kitchen> {

    @Mapping(source = "property.id", target = "propertyId")
    KitchenDTO toDto(Kitchen kitchen);

    @Mapping(source = "propertyId", target = "property")
    Kitchen toEntity(KitchenDTO kitchenDTO);

    default Kitchen fromId(Long id) {
        if (id == null) {
            return null;
        }
        Kitchen kitchen = new Kitchen();
        kitchen.setId(id);
        return kitchen;
    }
}
