package in.walkin.walkinhome.service.mapper;

import in.walkin.walkinhome.domain.*;
import in.walkin.walkinhome.service.dto.BathRoomDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity BathRoom and its DTO BathRoomDTO.
 */
@Mapper(componentModel = "spring", uses = {PropertyMapper.class})
public interface BathRoomMapper extends EntityMapper<BathRoomDTO, BathRoom> {

    @Mapping(source = "property.id", target = "propertyId")
    BathRoomDTO toDto(BathRoom bathRoom);

    @Mapping(source = "propertyId", target = "property")
    BathRoom toEntity(BathRoomDTO bathRoomDTO);

    default BathRoom fromId(Long id) {
        if (id == null) {
            return null;
        }
        BathRoom bathRoom = new BathRoom();
        bathRoom.setId(id);
        return bathRoom;
    }
}
