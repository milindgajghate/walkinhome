package in.walkin.walkinhome.service.mapper;

import in.walkin.walkinhome.domain.*;
import in.walkin.walkinhome.service.dto.DiscountDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity Discount and its DTO DiscountDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DiscountMapper extends EntityMapper<DiscountDTO, Discount> {



    default Discount fromId(Long id) {
        if (id == null) {
            return null;
        }
        Discount discount = new Discount();
        discount.setId(id);
        return discount;
    }
}
