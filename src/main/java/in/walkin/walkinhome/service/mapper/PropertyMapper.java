package in.walkin.walkinhome.service.mapper;

import in.walkin.walkinhome.domain.*;
import in.walkin.walkinhome.service.dto.PropertyDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Property and its DTO PropertyDTO.
 */
@Mapper(componentModel = "spring", uses = {DiscountMapper.class, AddressMapper.class, PropertyDetailsMapper.class, BathRoomMapper.class,
BedRoomMapper.class, KitchenMapper.class, LivingRoomMapper.class, ImageMapper.class})
public interface PropertyMapper extends EntityMapper<PropertyDTO, Property> {

    @Mapping(source = "discount", target = "discount")
    @Mapping(source = "address", target = "address")
    @Mapping(source = "propertyDetails", target = "propertyDetails")
    @Mapping(source = "bathRooms", target = "bathRooms")
    @Mapping(source = "bedRooms", target = "bedRooms")
    @Mapping(source = "kitchens", target = "kitchens")
    @Mapping(source = "livingRooms", target = "livingRooms")
    @Mapping(source = "images", target = "images")
    PropertyDTO toDto(Property property);

    @Mapping(source = "discount", target = "discount")
    @Mapping(source = "address", target = "address")
    @Mapping(source = "propertyDetails", target = "propertyDetails")
    @Mapping(source = "bathRooms", target = "bathRooms")
    @Mapping(source = "bedRooms", target = "bedRooms")
    @Mapping(source = "kitchens", target = "kitchens")
    @Mapping(source = "livingRooms", target = "livingRooms")
    @Mapping(source = "images", target = "images")
    Property toEntity(PropertyDTO propertyDTO);

    default Property fromId(Long id) {
        if (id == null) {
            return null;
        }
        Property property = new Property();
        property.setId(id);
        return property;
    }
}
