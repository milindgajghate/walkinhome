package in.walkin.walkinhome.service.mapper;

import in.walkin.walkinhome.domain.*;
import in.walkin.walkinhome.service.dto.LivingRoomDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity LivingRoom and its DTO LivingRoomDTO.
 */
@Mapper(componentModel = "spring", uses = {PropertyMapper.class})
public interface LivingRoomMapper extends EntityMapper<LivingRoomDTO, LivingRoom> {

    @Mapping(source = "property.id", target = "propertyId")
    LivingRoomDTO toDto(LivingRoom livingRoom);

    @Mapping(source = "propertyId", target = "property")
    LivingRoom toEntity(LivingRoomDTO livingRoomDTO);

    default LivingRoom fromId(Long id) {
        if (id == null) {
            return null;
        }
        LivingRoom livingRoom = new LivingRoom();
        livingRoom.setId(id);
        return livingRoom;
    }
}
