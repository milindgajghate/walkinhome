package in.walkin.walkinhome.service.mapper;

import in.walkin.walkinhome.domain.*;
import in.walkin.walkinhome.service.dto.TenantDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Tenant and its DTO TenantDTO.
 */
@Mapper(componentModel = "spring", uses = {AddressMapper.class})
public interface TenantMapper extends EntityMapper<TenantDTO, Tenant> {

    @Mapping(source = "permAddress.id", target = "permAddressId")
    @Mapping(source = "tempAddress.id", target = "tempAddressId")
    TenantDTO toDto(Tenant tenant);

    @Mapping(source = "permAddressId", target = "permAddress")
    @Mapping(source = "tempAddressId", target = "tempAddress")
    Tenant toEntity(TenantDTO tenantDTO);

    default Tenant fromId(Long id) {
        if (id == null) {
            return null;
        }
        Tenant tenant = new Tenant();
        tenant.setId(id);
        return tenant;
    }
}
