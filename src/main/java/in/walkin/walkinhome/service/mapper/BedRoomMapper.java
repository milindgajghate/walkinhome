package in.walkin.walkinhome.service.mapper;

import in.walkin.walkinhome.domain.*;
import in.walkin.walkinhome.service.dto.BedRoomDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity BedRoom and its DTO BedRoomDTO.
 */
@Mapper(componentModel = "spring", uses = {DiscountMapper.class, PropertyMapper.class, BedMapper.class})
public interface BedRoomMapper extends EntityMapper<BedRoomDTO, BedRoom> {

    @Mapping(source = "discount", target = "discount")
    @Mapping(source = "property.id", target = "propertyId")
    @Mapping(source = "bedIds", target = "beds")
    BedRoomDTO toDto(BedRoom bedRoom);

    @Mapping(source = "discount", target = "discount")
    @Mapping(source = "beds", target =  "bedIds")
    @Mapping(source = "propertyId", target = "property")
    BedRoom toEntity(BedRoomDTO bedRoomDTO);

    default BedRoom fromId(Long id) {
        if (id == null) {
            return null;
        }
        BedRoom bedRoom = new BedRoom();
        bedRoom.setId(id);
        return bedRoom;
    }
}
