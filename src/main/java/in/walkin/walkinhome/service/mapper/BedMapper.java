package in.walkin.walkinhome.service.mapper;

import in.walkin.walkinhome.domain.*;
import in.walkin.walkinhome.service.dto.BedDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Bed and its DTO BedDTO.
 */
@Mapper(componentModel = "spring", uses = {DiscountMapper.class, BedRoomMapper.class})
public interface BedMapper extends EntityMapper<BedDTO, Bed> {

    @Mapping(source = "discount", target = "discount")
    @Mapping(source = "bedRoom.id", target = "bedRoom")
    BedDTO toDto(Bed bed);

    @Mapping(source = "discount", target = "discount")
    @Mapping(source = "bedRoom", target = "bedRoom.id")
    Bed toEntity(BedDTO bedDTO);

    default Bed fromId(Long id) {
        if (id == null) {
            return null;
        }
        Bed bed = new Bed();
        bed.setId(id);
        return bed;
    }
}
