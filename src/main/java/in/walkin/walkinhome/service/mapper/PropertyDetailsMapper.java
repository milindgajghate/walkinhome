package in.walkin.walkinhome.service.mapper;

import in.walkin.walkinhome.domain.*;
import in.walkin.walkinhome.service.dto.PropertyDetailsDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity PropertyDetails and its DTO PropertyDetailsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PropertyDetailsMapper extends EntityMapper<PropertyDetailsDTO, PropertyDetails> {



    default PropertyDetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        PropertyDetails propertyDetails = new PropertyDetails();
        propertyDetails.setId(id);
        return propertyDetails;
    }
}
