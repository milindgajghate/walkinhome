export interface IAddress {
    id?: number;
    houseOrFlatNo?: string;
    building?: string;
    societyOrStreet?: string;
    area?: string;
    pinCode?: string;
    city?: string;
    district?: string;
    state?: string;
    country?: string;
}

export class Address implements IAddress {
    constructor(
        public id?: number,
        public houseOrFlatNo?: string,
        public building?: string,
        public societyOrStreet?: string,
        public area?: string,
        public pinCode?: string,
        public city?: string,
        public district?: string,
        public state?: string,
        public country?: string
    ) {}
}
