import {IDiscount} from "./discount.model";

export const enum Status {
    BOOKED = 'BOOKED',
    AVAILABLE = 'AVAILABLE'
}

export interface IBed {
    id?: number;
    status?: Status;
    price?: number;
    discountId?: IDiscount;
    bedRoomId?: number;
}

export class Bed implements IBed {
    constructor(public id?: number, public status?: Status, public price?: number, public discountId?: IDiscount, public bedRoomId?: number) {}
}
