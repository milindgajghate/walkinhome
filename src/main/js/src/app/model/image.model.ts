export const enum RoomType {
    KITCHEN = 'KITCHEN',
    BEDROOM = 'BEDROOM',
    BATHROOM = 'BATHROOM'
}

export interface IImage {
    id?: number;
    roomTypeId?: RoomType;
    hostUrl?: string;
    fileName?: string;
    propertyId?: number;
}

export class Image implements IImage {
    constructor(
        public id?: number,
        public roomTypeId?: RoomType,
        public hostUrl?: string,
        public fileName?: string,
        public propertyId?: number
    ) {}
}
