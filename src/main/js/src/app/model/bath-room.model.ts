export interface IBathRoom {
    id?: number;
    isAttachedType?: boolean;
    isWesternToilet?: boolean;
    isGeyser?: boolean;
    isFullDayWater?: boolean;
    propertyId?: number;
}

export class BathRoom implements IBathRoom {
    constructor(
        public id?: number,
        public isAttachedType?: boolean,
        public isWesternToilet?: boolean,
        public isGeyser?: boolean,
        public isFullDayWater?: boolean,
        public propertyId?: number
    ) {
        this.isAttachedType = this.isAttachedType || false;
        this.isWesternToilet = this.isWesternToilet || false;
        this.isGeyser = this.isGeyser || false;
        this.isFullDayWater = this.isFullDayWater || false;
    }
}
