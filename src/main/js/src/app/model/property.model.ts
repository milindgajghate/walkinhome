import { IBathRoom } from './bath-room.model';
import { IBedRoom } from './bed-room.model';
import { IKitchen } from './kitchen.model';
import { ILivingRoom } from './living-room.model';
import { IImage } from './image.model';
import {IDiscount} from "./discount.model";
import {IAddress} from "./address.model";
import {IPropertyDetails} from "./property-details.model";

export const enum Status {
    BOOKED = 'BOOKED',
    AVAILABLE = 'AVAILABLE'
}

export const enum TenantType {
    GIRL = 'GIRL',
    BOY = 'BOY',
    FAMILY = 'FAMILY'
}

export const enum FoodPref {
    VEG = 'VEG',
    NON_VEG = 'NON_VEG'
}

export interface IProperty {
    id?: number;
    status?: Status;
    area?: number;
    desc?: string;
    carParkingNo?: number;
    twoWheelerParkingNo?: number;
    price?: number;
    builtYear?: number;
    floor?: number;
    tenantType?: TenantType;
    foodPref?: FoodPref;
    isSecurity?: boolean;
    isLift?: boolean;
    discountId?: IDiscount;
    addressId?: IAddress;
    propertyDetailsId?: IPropertyDetails;
    bathRooms?: IBathRoom[];
    bedRooms?: IBedRoom[];
    kitchens?: IKitchen[];
    livingRooms?: ILivingRoom[];
    images?: IImage[];
}

export class Property implements IProperty {
    constructor(
        public id?: number,
        public status?: Status,
        public area?: number,
        public desc?: string,
        public carParkingNo?: number,
        public twoWheelerParkingNo?: number,
        public price?: number,
        public builtYear?: number,
        public floor?: number,
        public tenantType?: TenantType,
        public foodPref?: FoodPref,
        public isSecurity?: boolean,
        public isLift?: boolean,
        public discountId?: IDiscount,
        public addressId?: IAddress,
        public propertyDetailsId?: IPropertyDetails,
        public bathRooms?: IBathRoom[],
        public bedRooms?: IBedRoom[],
        public kitchens?: IKitchen[],
        public livingRooms?: ILivingRoom[],
        public images?: IImage[]
    ) {
        this.isSecurity = this.isSecurity || false;
        this.isLift = this.isLift || false;
    }
}
