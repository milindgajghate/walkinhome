export interface IKitchen {
    id?: number;
    isDining?: boolean;
    isFridge?: boolean;
    isGasStove?: boolean;
    isModular?: boolean;
    isInduction?: boolean;
    isMicrovave?: boolean;
    isWaterFilter?: boolean;
    isCrockery?: boolean;
    isToaster?: boolean;
    isGrinder?: boolean;
    isRiceCooker?: boolean;
    isLpg?: boolean;
    propertyId?: number;
}

export class Kitchen implements IKitchen {
    constructor(
        public id?: number,
        public isDining?: boolean,
        public isFridge?: boolean,
        public isGasStove?: boolean,
        public isModular?: boolean,
        public isInduction?: boolean,
        public isMicrovave?: boolean,
        public isWaterFilter?: boolean,
        public isCrockery?: boolean,
        public isToaster?: boolean,
        public isGrinder?: boolean,
        public isRiceCooker?: boolean,
        public isLpg?: boolean,
        public propertyId?: number
    ) {
        this.isDining = this.isDining || false;
        this.isFridge = this.isFridge || false;
        this.isGasStove = this.isGasStove || false;
        this.isModular = this.isModular || false;
        this.isInduction = this.isInduction || false;
        this.isMicrovave = this.isMicrovave || false;
        this.isWaterFilter = this.isWaterFilter || false;
        this.isCrockery = this.isCrockery || false;
        this.isToaster = this.isToaster || false;
        this.isGrinder = this.isGrinder || false;
        this.isRiceCooker = this.isRiceCooker || false;
        this.isLpg = this.isLpg || false;
    }
}
