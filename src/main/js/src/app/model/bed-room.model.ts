import { IBed } from './bed.model';
import {IDiscount} from "./discount.model";

export const enum Status {
    BOOKED = 'BOOKED',
    AVAILABLE = 'AVAILABLE'
}

export interface IBedRoom {
    id?: number;
    status?: Status;
    isStudyTable?: boolean;
    isCupBoard?: boolean;
    isTerrace?: boolean;
    isAcBoolean?: boolean;
    price?: number;
    discountId?: IDiscount;
    bedIds?: IBed[];
    propertyId?: number;
}

export class BedRoom implements IBedRoom {
    constructor(
        public id?: number,
        public status?: Status,
        public isStudyTable?: boolean,
        public isCupBoard?: boolean,
        public isTerrace?: boolean,
        public isAcBoolean?: boolean,
        public price?: number,
        public discountId?: IDiscount,
        public bedIds?: IBed[],
        public propertyId?: number
    ) {
        this.isStudyTable = this.isStudyTable || false;
        this.isCupBoard = this.isCupBoard || false;
        this.isTerrace = this.isTerrace || false;
        this.isAcBoolean = this.isAcBoolean || false;
    }
}
