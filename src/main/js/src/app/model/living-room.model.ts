export interface ILivingRoom {
    id?: number;
    isSofa?: boolean;
    isTv?: boolean;
    isWashMachine?: boolean;
    isDishTv?: boolean;
    isWifi?: boolean;
    isAc?: boolean;
    propertyId?: number;
}

export class LivingRoom implements ILivingRoom {
    constructor(
        public id?: number,
        public isSofa?: boolean,
        public isTv?: boolean,
        public isWashMachine?: boolean,
        public isDishTv?: boolean,
        public isWifi?: boolean,
        public isAc?: boolean,
        public propertyId?: number
    ) {
        this.isSofa = this.isSofa || false;
        this.isTv = this.isTv || false;
        this.isWashMachine = this.isWashMachine || false;
        this.isDishTv = this.isDishTv || false;
        this.isWifi = this.isWifi || false;
        this.isAc = this.isAc || false;
    }
}
