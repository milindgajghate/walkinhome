export interface IPropertyDetails {
    id?: number;
    noOfFloors?: number;
    nearbyHospital?: number;
    nearbyShop?: number;
    nearbyBusstop?: number;
}

export class PropertyDetails implements IPropertyDetails {
    constructor(
        public id?: number,
        public noOfFloors?: number,
        public nearbyHospital?: number,
        public nearbyShop?: number,
        public nearbyBusstop?: number
    ) {}
}
