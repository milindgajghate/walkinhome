export class SearchCriteria {
  keyword: string;
  city: string;
  propertyFor: number;
  priceRange: number[];
  nearByBusstop: number[];
  nearByShop: number[];
  nearByHospital: number[];
  isTwoWheelerParking = false;
  isCarParking = false;
  isStrictlyVeg = false;
  isSecurity = false;
  isLift = false;
  isAc = false;
  isAttachedBathroom = false;
  isJogPath = false;
  isGym = false;
}
