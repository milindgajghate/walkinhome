export interface PropertyBasicDetails {
  id: number;
  location: string;
  gender: string;
  bedsavailable: number;
  image: string;
}
