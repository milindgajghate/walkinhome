import { Moment } from 'moment';

export interface IDiscount {
    id?: number;
    promoCode?: string;
    desc?: string;
    startDate?: Moment;
    endDate?: Moment;
    discountPct?: number;
}

export class Discount implements IDiscount {
    constructor(
        public id?: number,
        public promoCode?: string,
        public desc?: string,
        public startDate?: Moment,
        public endDate?: Moment,
        public discountPct?: number
    ) {}
}
