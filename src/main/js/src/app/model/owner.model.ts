export interface IOwner {
    id?: number;
    firstName?: string;
    middleName?: string;
    lastName?: string;
    phoneNumber?: string;
    homeNumber?: string;
    mobileNumber?: string;
    permAddressId?: number;
    tempAddressId?: number;
}

export class Owner implements IOwner {
    constructor(
        public id?: number,
        public firstName?: string,
        public middleName?: string,
        public lastName?: string,
        public phoneNumber?: string,
        public homeNumber?: string,
        public mobileNumber?: string,
        public permAddressId?: number,
        public tempAddressId?: number
    ) {}
}
