import { Moment } from 'moment';

export interface IBooking {
    id?: number;
    bookingStartDate?: Moment;
    bookingEndDate?: Moment;
    price?: number;
    bedId?: number;
    tenantId?: number;
}

export class Booking implements IBooking {
    constructor(
        public id?: number,
        public bookingStartDate?: Moment,
        public bookingEndDate?: Moment,
        public price?: number,
        public bedId?: number,
        public tenantId?: number
    ) {}
}
