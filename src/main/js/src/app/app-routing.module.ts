import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {NavbarComponent, MainComponent, FooterComponent} from './layouts';
import {PropertyDetailsComponent} from './property-details/property-details.component';
import {PropertiesComponent} from './properties/properties.component';

const routes: Routes = [
    { path: '', redirectTo: 'main', pathMatch: 'full' },
    { path: 'main', component: MainComponent },
    { path: 'propertydetails', component: PropertyDetailsComponent },
    { path: 'properties', component: PropertiesComponent },
    { path: 'propertydetail/:id', component: PropertyDetailsComponent },

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }
