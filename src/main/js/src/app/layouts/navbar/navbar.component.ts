import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['navbar.css']
})
export class NavbarComponent implements OnInit {

  inProduction: boolean;
    isNavbarCollapsed: boolean;
    languages: any[];
    swaggerEnabled: boolean;
    version: string;

    constructor(
    ) {}

    ngOnInit() {
    }

    collapseNavbar() {
    }

    isAuthenticated() {
    }

    login() {
    }

    logout() {
    }

    toggleNavbar() {
    }

    getImageUrl() {
    }

}
