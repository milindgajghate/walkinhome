import {Component, Inject, Input, OnInit, ViewChild} from '@angular/core';
import {PropertyBasicDetails} from '../../model/property-basic-details';
import {FormsModule} from '@angular/forms';
import {SearchCriteria} from '../../model/search-criteria';
import { Router } from '@angular/router';
import {PropertiesComponent} from '../../properties/properties.component';
import {MainService} from './main.service';

@Component({
  selector: 'main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  @Input() searchCriteria: SearchCriteria;
  topProperties: PropertyBasicDetails[] = [];
  loadingIndicator = false;
  constructor(private mainService: MainService,
              private router: Router) {
  }

  onChange(event: any) {
    console.log(event);
  }

  ngOnInit() {
    this.getTopProperties();
    this.searchCriteria = new SearchCriteria();
    this.searchCriteria.nearByBusstop = [200, 3000];
    this.searchCriteria.nearByShop = [200, 3000];
    this.searchCriteria.nearByHospital = [200, 3000];
    this.searchCriteria.priceRange = [200, 50000];
    this.searchCriteria.isJogPath = true;
    console.log(this.topProperties);
  }
  private getTopProperties() {
    this.mainService.getTopProperties((data) => {
      this.topProperties = data;
      setTimeout(() => {
        this.loadingIndicator = false;
      }, 1500);
    });
  }

  coverage() {
  }

  onSubmit() {
    console.log(this.searchCriteria.nearByBusstop);
    console.log(this.searchCriteria.nearByShop);
    console.log(this.searchCriteria.nearByHospital);
    console.log(this.searchCriteria.priceRange);
    console.log(this.searchCriteria.isJogPath);
    this.router.navigate(['/properties']);
  }

}
