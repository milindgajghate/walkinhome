import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MainService {


  constructor() { }

  fetchTaskData(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', '../../../assets/data/property-initial-details.json');
    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }

  getTopProperties(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', '../../../assets/data/property-initial-details.json');
    req.onload = () => {
      cb(JSON.parse(req.response));
    };
    req.send();
  }

}
