import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; import { HttpModule } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class PropertiesService {

  constructor() { }

  fetchTaskData(cb, searchCriteria) {
    const req = new XMLHttpRequest();
    req.open('GET', '../../../assets/data/property-initial-details.json');
    req.onload = () => {
      cb(JSON.parse(req.response));
    };
    req.send();
  }

  getTopProperties(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', '../../../assets/data/property-initial-details.json');
    req.onload = () => {
      cb(JSON.parse(req.response));
    };
    req.send();
  }

  searchProperties(cb, searchCriteria) {
    const req = new XMLHttpRequest();
    req.open('GET', '../../../assets/data/property-initial-details.json');
    req.onload = () => {
      cb(JSON.parse(req.response));
    };
    req.send();
  }
}
