import {Component, Input, OnInit} from '@angular/core';
import {PropertyBasicDetails} from '../model/property-basic-details';
import {MainService} from '../layouts/main/main.service';
import {SearchCriteria} from '../model/search-criteria';
import {Router} from '@angular/router';

@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styleUrls: ['./properties.component.css']
})
export class PropertiesComponent implements OnInit {
  dataList: PropertyBasicDetails[] = [];
  topPropertyList: PropertyBasicDetails[] = [];
  loadingIndicator = false;
  @Input() searchCriteria: SearchCriteria;
  constructor(private mainService: MainService,
              private router: Router) { }

  ngOnInit() {
    this.loadInitialData();
    this.searchCriteria = new SearchCriteria();
    this.searchCriteria.nearByBusstop = [200, 3000];
    this.searchCriteria.nearByShop = [200, 3000];
    this.searchCriteria.nearByHospital = [200, 3000];
    this.searchCriteria.priceRange = [200, 50000];
  }
  private loadInitialData() {
    this.mainService.fetchTaskData((data) => {
      this.dataList = data;
      setTimeout(() => {
        this.loadingIndicator = false;
      }, 1500);
    });
  }

  private getTopProperties() {
    this.mainService.getTopProperties((data) => {
      this.topPropertyList = data;
      setTimeout(() => {
        this.loadingIndicator = false;
      }, 1500);
    });
  }



  onSubmit() {
    console.log(this.searchCriteria.nearByBusstop);
    console.log(this.searchCriteria.nearByShop);
    console.log(this.searchCriteria.nearByHospital);
    console.log(this.searchCriteria.priceRange);
    console.log(this.searchCriteria.isJogPath);
    this.router.navigateByUrl('/properties');
    /*this.router.navigate(['/properties']);*/
  }
  onChange(event: any) {
    console.log(event);
  }
}
